import org.jumpmind.symmetric.csv.CsvReader;
import org.jumpmind.symmetric.csv.CsvWriter;

import java.io.*;
import java.util.Properties;

/**
 * Created by dokannon on 05-05-17.
 */
public class ProcesaCSVMain {

    public static void main (String... args){

        String nombreFichero;
        boolean existe;
        Properties props = new Properties();
        String path = Props.getInstance().getValue("path_archivos_a_procesar");
        System.out.println("path : " + path);
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        props.getClass().getResourceAsStream("/path.properties");
        File archivoProcesados = new File ("/archivos_procesados.txt");
        FileWriter fichero = null;
        PrintWriter pw = null;
        int aux = 1;
        if (archivoProcesados.exists()){
            existe= true;
        }


        for (int i = 0; i < listOfFiles.length; i++)
        {

            if (listOfFiles[i].isFile())
            {
                files = listOfFiles[i].getName();
                if (files.endsWith(".csv") &&!files.startsWith("Bitacora") )
                {
                    if (files.startsWith("ViajeDos")){

                    }else if(files.startsWith("PasoPorPuntos")){

                    }
                    try {
                    //    CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
                        CsvReader reader = new CsvReader("/home/dtpm/archivos_a_procesar/"+files, '|');
                        while(reader.readRecord()){
                                 String[] array = reader.getHeaders();
                                if (reader.get(0).equalsIgnoreCase("UN")){
                                nombreFichero="/home/dtpm/"+files;

                                File f = new File(nombreFichero);

                                if(f.exists()) {
                                    CsvWriter csvOutput = new CsvWriter(new FileWriter(nombreFichero, true), '|');
                                    csvOutput.write(reader.get(0));
                                    csvOutput.write(reader.get(1));
                                    csvOutput.write(reader.get(2));
                                    csvOutput.write(reader.get(3));
                                    csvOutput.write(reader.get(4));
                                    csvOutput.write(reader.get(5));
                                    csvOutput.write(reader.get(6));
                                    csvOutput.write(reader.get(7));
                                    csvOutput.write(reader.get(8));
                                    csvOutput.write(reader.get(9));
                                    csvOutput.write(reader.get(10));
                                    csvOutput.write(reader.get(11));
                                    csvOutput.write(reader.get(12));
                                    csvOutput.write(reader.get(13));
                                    csvOutput.write(reader.get(14));
                                    csvOutput.write(reader.get(15));
                                    csvOutput.write(reader.get(16));
                                    csvOutput.endRecord();
                                    csvOutput.close();
                                }else {
                                    CsvWriter csvOutput = new CsvWriter(new FileWriter(nombreFichero, false), '|');
                                    csvOutput.writeRecord(array);

                                    csvOutput.endRecord();
                                    csvOutput.close();
                                }


                            }
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }
        }

    }

}
