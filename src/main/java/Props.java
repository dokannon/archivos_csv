import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;


/**
 * Created by dokannon on 05-05-17.
 */
public class Props {

    private static final Logger log = Logger.getLogger(Props.class);

    private static Props instance = null;
    private Properties properties;

    private Props() {

        properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/path.properties"));
        } catch (IOException e) {
            log.error("", e);

        }

    }

    public static Props getInstance() {
        if(instance == null) {
            instance = new Props();
        }
        return instance;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }

}
