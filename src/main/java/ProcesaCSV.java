import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dokannon on 05-05-17.
 */
public class ProcesaCSV {

    public static void main(String... args) throws IOException {

        String headerPasosPorPuntos = "UN|Servicio|Sntd|Patente|FH Cabezal Virtual|Franja|FH Inicio|ID|Pto|Desc. Pto|FH Pto|KM Pto|Huso Horario";
        String headerViajeDos = "UN|Servicio|Sentido|Patente|FH Inicio|ID Viaje|FH ultimo PCV|PCV Reales|PCV Ruta|Primer PCV|Ultimo PCV|C2|C4|C5|Franja|FH Fin|Largo Ini|Largo Fin|KM|Ruta Ini|Ruta Fin|Desc UN|Plazas|Huso Horario|Nro. Pto. Medio|FH Pto. Medio|KM. Pto. Medio|Ruta Pto. Medio|Tipo Pto. Medio|KM Cabezal Virtual|FH Cabezal Virtual|CV Interpolado|Nro. Pto. 20%|FH Pto. 20%|KM Pto. 20%|Ruta Pto. 20%|Tipo Pto. 20%|Nro. Pto. 40%|FH Pto. 40%|KM Pto. 40%|Ruta Pto. 40%|Tipo Pto. 40%|Nro. Pto. 70%|FH Pto. 70%|KM Pto. 70%|Ruta Pto. 70%|Tipo Pto. 70%|Nro. Pto. 85%|FH Pto. 85%|KM Pto. 85%|Ruta Pto. 85%|Tipo Pto. 85%";
        String viajesDos = "ViajeDos";
        String pasosPorPuntos = "PasoPorPuntos";
        String mainFolderPath = "/home/dtpm/archivos_a_procesar";
        Map<String, Object> map = new HashMap<String, Object>();
        File mainFolder = new File(mainFolderPath);
        if (!mainFolder.exists() || !mainFolder.isDirectory()) return;

        for (File file : mainFolder.listFiles()) {
            if (file.getName().startsWith(viajesDos) || file.getName().startsWith(pasosPorPuntos)) {

                if (map.containsKey(file.getName())) continue;

                FileReader reader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String sCurrentLine;

                while  ((sCurrentLine = bufferedReader.readLine()) != null) {

                    String un = getUnidadNegocio(sCurrentLine);
                    File archivo = new File(mainFolderPath + File.separator + un + File.separator + file.getName());

                    if (!archivo.exists()) {
                        archivo.getParentFile().mkdirs();
                        FileWriter fw = new FileWriter(archivo);
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(sCurrentLine);
                        bw.close();
                        fw.close();
                    }else {
                        FileWriter fw = new FileWriter(archivo, true);
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(sCurrentLine);
                        bw.close();
                        fw.close();
                    }

                    break;

                }



                reader.close();



//                System.out.println(file.getName());
            }

        }


    }

    private static String getUnidadNegocio(String linea){
        return linea.split("[|]")[0];
    }

}
